import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Todo } from './Todo';
import { catchError, map, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TodoService {
  private baseURL = 'http://localhost:8090/todos';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  getAllTodos(): Observable<Todo[]> {
    return this.http
      .get<Todo[]>(this.baseURL)
      .pipe(catchError(this.handleError<Todo[]>('getAllTodos', [])));
  }

  getTodoByTodoId(todoId: string) {
    return this.http
      .get<Todo>(`${this.baseURL}/${todoId}`)
      .pipe(catchError(this.handleError<Todo>('getTodoByTodoId')));
  }

  postTodo(todo: Todo): Observable<Todo> {
    return this.http
      .post<Todo>(this.baseURL, todo, this.httpOptions)
      .pipe(catchError(this.handleError<Todo>('postHero')));
  }

  deleteTodoByName(name: string): Observable<Todo> {
    const url = `${this.baseURL}/${name}`;
    return this.http
      .delete<Todo>(url, this.httpOptions)
      .pipe(catchError(this.handleError<Todo>('deleteHero')));
  }

  updateTodo(toUpdateTodo: Todo): Observable<any> {
    return this.http
      .put(`${this.baseURL}`, toUpdateTodo, this.httpOptions)
      .pipe(catchError(this.handleError<any>('updateHero')));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  constructor(private http: HttpClient) {}
}
