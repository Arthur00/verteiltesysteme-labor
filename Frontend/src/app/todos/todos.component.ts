import { Component, OnInit } from '@angular/core';
import { Todo } from '../Todo';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css'],
})
export class TodosComponent implements OnInit {
  todos?: Todo[];
  todoId?: string;
  selectedTodo?: Todo;
  foundTodo?: Todo;
  displayedColumns: string[] = ['todo', 'priority'];

  onSave(selectedTodo: Todo) {
    this.selectedTodo = selectedTodo;
    this.todoService.updateTodo(selectedTodo).subscribe();
  }
  searchTodo(todoId: string) {
    this.todoService.getTodoByTodoId(todoId).subscribe((todo) => {
      this.foundTodo = todo;
    });
  }
  getAllTodos() {
    this.todoService.getAllTodos().subscribe((todos) => (this.todos = todos));
  }
  addNewTodo(name: string, prio: string) {
    if (!name) return;
    let newTodo: Todo;
    newTodo = {
      todo: name,
      priority: parseInt(prio),
    };
    this.todoService.postTodo(newTodo).subscribe((todo) => {
      this.todos?.push(todo);
    });
  }
  deleteTodo(name: string) {
    this.todoService.deleteTodoByName(name).subscribe(() => {
      this.getAllTodos();
    });
  }
  constructor(private todoService: TodoService) {}

  ngOnInit(): void {
    this.getAllTodos();
  }
}
