# Verteilte Systeme Labor

This programm was made as the lab part of the class distributed systems.

## How to run the programm

1. Copy the project to your local directory
2. Pull the two needed images from this repo: https://hub.docker.com/u/arthur6911
3. In the root directory of the project, run the command: docker-compose up -d 

Now the angular webapplication is avialable under localhost and the spring-boot api is avialable under localhost:8090.
