package com.example.demo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface TodoRepository extends CrudRepository<ToDoItem, String>  {
    ToDoItem findByTodo(String name); 
}
