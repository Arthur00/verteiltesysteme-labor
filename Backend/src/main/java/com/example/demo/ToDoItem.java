package com.example.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ToDoItem")
public class ToDoItem {
    @Id
    @Column(name = "todo")
    String todo; 

    @Column(name = "priority")
    int priority; 

    public ToDoItem(){
        this.todo = "nothing"; 
        this.priority = 0; 
    }
    public ToDoItem(String todo){
        this.todo = todo; 
    }
    public ToDoItem(String todo, int priority){
        this.todo = todo; 
        this.priority = priority; 
    }
    public void setPriority(int priority){
        this.priority = priority; 
    }
    public int getPriority(){
        return priority;
    }
    public void setTodo(String todo){
        this.todo = todo; 
    }
    public String getTodo(){
        return todo; 
    }
}
