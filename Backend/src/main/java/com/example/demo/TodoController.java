package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.models.media.MediaType;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Consumer;

@CrossOrigin(origins = "*")
@SpringBootApplication
@RestController
@EnableJpaRepositories
public class TodoController {

	@Autowired TodoRepository todoRepository; 
	
		@GetMapping("/todos")
		public List<ToDoItem> getTodos() {
			return (List<ToDoItem>) todoRepository.findAll(); 
		}

		@GetMapping("/todos/{name}")
		public ToDoItem getUsers(@PathVariable String name) {
			if (todoRepository.existsById(name))
				return todoRepository.findByTodo(name);
			else 
				return null; 
		}

    	@PostMapping(consumes = "application/json", produces = "application/json", value = "/todos")
		public ToDoItem postTodos(@RequestBody ToDoItem toDoItem){
			todoRepository.save(toDoItem); 
			return toDoItem; 
		}
		
		@DeleteMapping("/todos/{name}")
		public void deleteTodo(@PathVariable String name){
			if (todoRepository.existsById(name)){
				todoRepository.delete(todoRepository.findByTodo(name));
			}
		}
		@PutMapping("/todos")
		public ToDoItem updateTodo(@RequestBody ToDoItem toDoItem){
			if (todoRepository.existsById(toDoItem.getTodo())){
				ToDoItem tempTodo = new ToDoItem(toDoItem.getTodo(), toDoItem.getPriority());
				todoRepository.delete(todoRepository.findByTodo(toDoItem.getTodo()));
				todoRepository.save(tempTodo);
			}
			return todoRepository.findByTodo(toDoItem.getTodo()); 
		}
		public static void main(String[] args) {
			SpringApplication.run(TodoController.class, args);
		}
}
